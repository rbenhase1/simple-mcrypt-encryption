# Simple MCrypt Encryption for Gravity Forms #
Contributors: rbenhase

Donate link: http://ryanbenhase.com

Requires at least: 3.5

Tested up to: 4.2

Stable tag: 4.2

License: GPLv2 or later

License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin allows data to be encrypted using mcrypt; if the Gravity Forms plugin is also enabled, this plugin will also automatically encrypt all Gravity Forms fields before they are saved to the database. 

## Description ##

This plugin uses a random IV with MCRYPT_RIJNDAEL_128 and MCRYPT_MODE_CBC. You will need to supply it an encryption key by defining an MCRYPT_KEY constant somewhere in your PHP code. It is highly recommended that you put this in your wp-config.php file (see below), and that you protect this file (and your key) from being viewed or accessed by anyone else. You will, however, want to keep a copy of your key in a safe place; in the event that your key is lost, you will not be able to decrypt any data that it has been used to encrypt, and your data will essentially be lost.


## Requirements ##

* PHP 5.2 or higher
* WordPress 3.5 or above
* PHP's mcrypt extension 

## Requirements for Gravity Forms Integration ##

These are optional for simply using this plugin to encrypt/decrypt, but required if you want to automatically encrypt/decrypt Gravity Forms field data.

* Gravity Forms plugin
* MySQL 5 or higher
* mysqli extension

## Installation ##

This section describes how to install the plugin and get it working.

1. Upload the `simple-mcrypt-encryption` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Add your encryption key to wp-config.php (see below); do not lose this key.
4. Enable Gravity Forms automatic encryption (if desired) under Forms > Settings > Encryption.

## Adding Your Encryption Key ##

Your encryption key should be unique, and you should not share it with anyone. It is also important that you keep a copy of your key in a safe place; in the event that your key is lost, you will not be able to decrypt any of the data it has been used to encrypt.

If there is no encryption key set, the plugin will display an administrative notice when you are logged into the Wordpress dashboard. It will also supple a randomly-generated key that you can use if you'd like; just make sure you don't lose it. 

You will want to add a line like the following to your wp-config.php file in order to make things work:

    define('MCRYPT_KEY', 'your unique encryption key goes here');

## Changing Your Encryption Key ##

You should only change an existing encryption key if you know what you are doing. You will first need to un-encrypt all of your stored data (that is, anything encrypted in your database) using the existing/old key, then re-encrypt it using the new key. If you change your encryption key without doing this first, any data encrypted by the old key will be inaccessible. 

## Plugin Usage ##

This plugin allows you to decrypt or encrypt data within your PHP code. For example:


    if ( class_exists('MCrypt_Encryption' ) ) {
      echo MCrypt_Encryption::encrypt( 'This is the data to encrypt' );
    }

Or:

    if ( class_exists('MCrypt_Encryption' ) ) {
      echo MCrypt_Encryption::decrypt( 'enx:JKM3FFR4WP5HN6SG0C4ZAIF5K7H' );
    }

This plugin will also automatically encrypt and decrypt Gravity Forms form data if you enable the option found under Forms > Settings > Encryption. Only do this is you are sure you want your Gravity Forms data to be encrypted.  

## Final Warning ##

Once again, any data you encrypt with this plugin will not be able to be decrypted without using the same key. Once you set your key, you should keep a backup in a safe place, and you should not change it unless you first un-encrypt all of your stored data (i.e. anything encrypted inside your database) using the old key, then re-encrypt it with the new key. 
