<?php
/*
Plugin Name: Simple MCrypt Encryption for Gravity Forms
Plugin URI: 
Description: This plugin encrypts the data stored by Gravity forms (and potentially other Plugins) using MCrypt (symmetric encryption).
Version: 1.0.0
Author: Ryan Benhase
Author URI: http://www.ryanbenhase.com
*/

// Fix undefined function error
if(!function_exists('wp_get_current_user')) {
  include(ABSPATH . "wp-includes/pluggable.php");
}
      

/**
 * The main encryption class.
 */
class MCrypt_Encryption {
  
  /**
   * Encrypt a value.
   * 
   * @access public
   * @static
   * @param mixed $value The regular, unencrypted value.
   * @return String $value The encrypted value.
   */
  public static function encrypt( $value ) {
    if ( defined ( 'MCRYPT_KEY' ) ) {
      $salt     = "djkns(235mk^p";
      $password = "7%r?1C" . trim( MCRYPT_KEY ) . "jr-3";
      $key      = hash( 'SHA256', $salt . $password, true );
      srand();
      $iv = mcrypt_create_iv( mcrypt_get_iv_size( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC ), MCRYPT_RAND );
      if ( strlen( $iv_base64 = rtrim( base64_encode( $iv ), '=' ) ) != 22 )
          return false;
      $encrypted = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $key, $value . md5( $value ), MCRYPT_MODE_CBC, $iv ) );
      $value     = "enx:" . $iv_base64 . $encrypted;
    } // end if
    return $value;
  }
  
  /**
   * Decrypt a value.
   * 
   * @access public
   * @static
   * @param mixed $value The encrypted value.
   * @return String $value The decrypted value.
   */
  public static function decrypt( $value ) {
    if ( defined ( 'MCRYPT_KEY' ) ) {
      if ( strpos( $value, "enx:" ) !== false ) {
        $value     = str_replace( "enx:", "", $value );
        $salt      = "djkns(235mk^p";
        $password  = "7%r?1C" . trim( MCRYPT_KEY ) . "jr-3";
        $key       = hash( 'SHA256', $salt . $password, true );
        $iv        = base64_decode( substr( $value, 0, 22 ) . '==' );
        $encrypted = substr( $value, 22 );
        $decrypted = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $key, base64_decode( $encrypted ), MCRYPT_MODE_CBC, $iv ), "\0\4" );
        $hash      = substr( $decrypted, -32 );
        $decrypted = substr( $decrypted, 0, -32 );
        if ( md5( $decrypted ) != $hash )
            $value = '';
        $value = $decrypted;
      } //strpos( $value, "enx:" ) !== false
    } // end if
    
    return $value;
  }

} // end MCrypt_Encryption class


// If Gravity Forms Exists...
if ( class_exists( "GFForms" ) ) {
  GFForms::include_addon_framework();

  /**
   * The Gravity Forms add-on class.
   * 
   * @extends GFAddOn
   */
  class GFMCryptAddOn extends GFAddOn {
    protected $_version = "1.0"; 
    protected $_min_gravityforms_version = "1.7.9999";
    protected $_slug = "simplemcryptencryption";
    protected $_path = "simple-mcrypt-encryption/simple-mcrypt-encryption.php";
    protected $_full_path = __FILE__;
    protected $_title = "Simple MCrypt Encryption";
    protected $_short_title = "Encryption";
    
    
    public function init() {
      parent::init();
      
      $encryption_enabled = $this->get_plugin_setting("encryption_enabled");
      
      if ( $encryption_enabled ) {      
        add_filter( "gform_save_field_value", array( $this, "gform_save_field_value" ), 10, 4 );        
        add_filter( "gform_get_input_value", array( $this, "gform_get_field_value" ), 10, 4 );
      }
    }
    
    public function plugin_settings_fields() {
      
      if ( defined ( 'MCRYPT_KEY' ) ) { 
        // If encryption key is defined, enable setting to turn encryption on
        return 
        array(
            array(
                'title'       => 'Turning on MCrypt Encryption',
                'description' => '<p>When encryption is enabled, all Gravity Forms data will be encrypted before being saved to the database.<br><br>' .
                                 '<strong>You will not be able to decode encrypted data without the same encryption key you used to encrypt it.</strong><br>' .
                                 'You <strong>MUST</strong> keep a backup copy of your key in a safe place; if it is lost, your data will be inaccessible.</p>' . 
                                 '<p>For more, consult the <a href="' . plugins_url( 'readme.html', __FILE__ ) . '">README</a>.</p>',
                'fields'      => array(
                    array(
                        'label' => 'Enable Encryption on all Gravity Forms fields?',
                        'type'  => 'checkbox',
                        'name'  => 'encryption_enabled',
                        'choices' => array( 
                          array(
                            'label' => 'Enable',
                            'name' => 'encryption_enabled',
                            'default_value' => 0,
                            'tooltip' => 'Enabling this setting will cause all future form submissions to be encrypted. Before you do this, make sure you have a backup copy of your encryption key stored in a safe place.'
                          )
                        )
                    ),
                )
            ),
        );
      } else {
        // If encryption key is not defined
        
        // Generate a random key
        $key = md5( time() . rand( 100, 999 ) );
        
         return 
        array(
            array(
                'title'       => 'Turning on MCrypt Encryption',
                'description' => 'When encryption enabled, all Gravity Forms data will be encrypted before being saved to the database.<br><br>' .
                                 '<strong>You must first define an encryption key before you can turn on this setting.</strong><br>' .
                                 '<p>If you need a key, try adding this to your wp-config.php file:</p>' . 
                                 '<code>' . 
                                 '  define("MCRYPT_KEY", "' . $key . '");' . 
                                 '</code>' . 
                                 '<p><strong>Note: </strong>' . 
                                 'The key above ( ' . $key . ' ) was randomly generated. If you decide to use it, <strong>do not share it with anyone</strong>, and <strong>keep a backup copy in a safe place</strong>.<br>' . 
                                 '<br><strong>If you lose this key, you will not be able to decrypt any of the data you used it to encrypt. Don\'t lose your key!</strong>',
                'fields' => array()
            )
        );
      }      
    }
    
    /**
     * Function to save a modified Gravity Forms field value.
     * 
     * @access public
     * @param mixed $value The value to save
     * @param mixed $lead The lead
     * @param mixed $field The field to save
     * @param mixed $form The form to which the field belongs
     * @return String The modified field value to save
     */
    public function gform_save_field_value( $value, $lead, $field, $form ) {
      
      // Don't encrypt total fields
      if ( $field->type != 'total' && $field->type != 'product' ) {
      
        $new_value = '';
        
        if ( empty( $value ) ) {
            return $value;
        } //empty( $value )
        
        /* 
        Multi-input fields such as Name and Address will be represented as an array, 
        so each item needs to be encrypted individually
        */
        
        if ( is_array( $value ) ) {
          foreach ( $value as $key => $input_value ) {
            $value[$key] = MCrypt_Encryption::encrypt( $input_value );
          } //$value as $key => $input_value
          $new_value = $value;
        } //is_array( $value )
        $new_value = MCrypt_Encryption::encrypt( $value );
        
        return $new_value;
      } else {
        return $value;
      }
    }
      
    /**
     * Gets a field value for a Gravity Form.
     * 
     * @access public
     * @param mixed $value The value
     * @param mixed $lead The lead
     * @param mixed $field The field to retrieve value from
     * @param mixed $input_id The ID of the input 
     * @return void
     */
    public function gform_get_field_value( $value, $lead, $field, $input_id ) {
      global $wpdb;
      
      $detail = $wpdb->get_row( $wpdb->prepare( "SELECT id FROM " . $wpdb->prefix . "rg_lead_detail WHERE `lead_id` = %d AND `field_number` = %d", $lead['id'], $field['id'] ) );
      
      if ( is_object( $detail ) ) {
        $long = $wpdb->get_row( $wpdb->prepare( "SELECT value FROM " . $wpdb->prefix . "rg_lead_detail_long WHERE `lead_detail_id` = %d", $detail->id ) );
        
        if ( !empty( $long->value ) ) {
          $value = $long->value;
        } //!empty( $long->value )
      } //is_object( $detail )
      
      if ( empty( $value ) ) {
          return $value;
        } //empty( $value )
        
        /* 
        Multi-input fields such as Name and Address will be represented as an array, 
        so each item needs to be decrypted individually 
        */
        
      if ( is_array( $value ) ) {
        foreach ( $value as $key => $input_value ) {
          $value[$key] = MCrypt_Encryption::decrypt( $input_value );
        } //$value as $key => $input_value
        return $value;
      } //is_array( $value )
      
      return MCrypt_Encryption::decrypt( $value );
    }
        
          
  } // end GFMCrypt_AddOn class
  
  $mcrypt_addon = new GFMCryptAddOn();
  $mcrypt_addon->init();
}


// Run the plugin
if ( defined ( 'MCRYPT_KEY' ) ) { 
  $mcrypt_encryption_class = new MCrypt_Encryption();
} else {
  add_action( 'admin_notices', 'display_mcrypt_key_warning' );
}

function display_mcrypt_key_warning() {  
  echo '<div class="error">';
  echo '<h4>Simple MCrypt Encryption Warning</h4>';
  echo '<p>No encryption key is defined. '; 
  echo 'If you do not define an encryption key, this plugin will not encrypt or decrypt anything.<br>';
  echo 'For more, ';
  
  if ( class_exists( "GFForms" ) )
    echo 'visit the <a href="' . admin_url( 'admin.php?page=gf_settings&subview=simplemcryptencryption' ) . '">Settings page</a> or ';
    
  echo 'consult the <a href="' . plugins_url( 'readme.html', __FILE__ ) . '">README</a>.</p>';
  echo '</div>';
}

?>